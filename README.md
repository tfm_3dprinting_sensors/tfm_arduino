# tfm_arduino

:exclamation: Please, to use this repository start with doing the setup defined in https://gitlab.com/tfm_3dprinting_sensors/tfm_workspace_setup.

## Repository structure

This repository contains the Arduino code for several parts of my master's thesis.

- **quarter_bridge**: measure a sensor through a quarter configuration of the Wheatstone bridge.
- **double_quarter_bridge**: measure two sensors.
- **motor_drive_tension_cycle**: tension cycle with the testing machine to measure the sensor.
- **motor_drive_flexion_cycle**: flexion cycle with the testing machine to measure the sensor.
- **motor_fast_drive**: code to move the motor fast for motion validations.

## Authors and acknowledgment
Oriol Vendrell

## License
BSD 2-Clause "Simplified" License

## Project status
This project is part of my final master's thesis.
