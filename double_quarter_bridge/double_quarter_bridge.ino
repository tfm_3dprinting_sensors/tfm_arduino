#define VA A0
#define VB A1

#define VA2 A4
#define VB2 A5

int va_value = 0;
int vb_value = 0;

int va_value2 = 0;
int vb_value2 = 0;

void setup() {
  pinMode(VA, INPUT);
  pinMode(VB, INPUT);
  pinMode(VA2, INPUT);
  pinMode(VB2, INPUT);
  Serial.begin(9600);
}

void loop() {
  va_value = analogRead(VA);
  float va_msg = va_value*(5.0/1023.0);
  vb_value = analogRead(VB);
  float vb_msg = vb_value*(5.0/1023.0);

  va_value2 = analogRead(VA2);
  float va_msg2 = va_value2*(5.0/1023.0);
  vb_value2 = analogRead(VB2);
  float vb_msg2 = vb_value2*(5.0/1023.0);

  Serial.print(va_msg-vb_msg);
  //Serial.print(" Volts V");
  Serial.print(", ");
  Serial.println(va_msg2-vb_msg2);
  //Serial.println(" Volts V");
  
  delay(100);
}
