// defines pins numbers
#define stepPin 3
#define dirPin 4
#define limitSwitchMotor 11
#define limitSwitchSensor 10
#define stepsPerRevolution 200
#define fullStepResolution 1.8
#define startButton 7

#define VA A0
#define VB A1
#define V A2

int va_value = 0;
int vb_value = 0;
int v_value = 0;

bool running = false;
 
void setup() {
  // Sets the two pins as Outputs
  pinMode(stepPin,OUTPUT); 
  pinMode(dirPin,OUTPUT);
  pinMode(limitSwitchMotor,INPUT); 
  pinMode(limitSwitchSensor,INPUT);

  pinMode(VA, INPUT);
  pinMode(VB, INPUT);
  pinMode(V, INPUT);
  Serial.begin(9600);

  pinMode(startButton,INPUT);
}

void loop() {
  // Enables the motor to move in a particular direction
  // Makes 200 pulses for making one full cycle rotation
  if (running) {
    if (digitalRead(startButton) == HIGH) {
      running = false;
    }
    digitalWrite(dirPin, HIGH);
    for(int i = 0; i < 1; i++) {
      for(int x = 0; x < 1350; x++) {
      digitalWrite(stepPin,HIGH); 
      delayMicroseconds(500); 
      digitalWrite(stepPin,LOW); 
      delayMicroseconds(500);
      va_value = analogRead(VA);
      float va_msg = va_value*(5.0/1023.0);
      vb_value = analogRead(VB);
      float vb_msg = vb_value*(5.0/1023.0);
      Serial.print(va_msg-vb_msg);
      //v_value = analogRead(V);
      //float v_msg = v_value*(5.0/1023.0);
      //Serial.print(V);
      Serial.println(" Volts V ");
      }
    }
    for(int x = 0; x < 100; x++) {
      delayMicroseconds(500);
      va_value = analogRead(VA);
      float va_msg = va_value*(5.0/1023.0);
      vb_value = analogRead(VB);
      float vb_msg = vb_value*(5.0/1023.0);
      Serial.print(va_msg-vb_msg);
      //v_value = analogRead(V);
      //float v_msg = v_value*(5.0/1023.0);
      //Serial.print(V);
      Serial.println(" Volts V ");
    }
    delay(1000);
    digitalWrite(dirPin, LOW);
    for(int x = 0; x < 1350; x++) {
      digitalWrite(stepPin,HIGH); 
      delayMicroseconds(500); 
      digitalWrite(stepPin,LOW); 
      delayMicroseconds(500);
      va_value = analogRead(VA);
      float va_msg = va_value*(5.0/1023.0);
      vb_value = analogRead(VB);
      float vb_msg = vb_value*(5.0/1023.0);
      Serial.print(va_msg-vb_msg);
      //v_value = analogRead(V);
      //float v_msg = v_value*(5.0/1023.0);
      //Serial.print(V);
      Serial.println(" Volts V ");
    }
    delay(1000); // One second delay
  } else {
    if (digitalRead(startButton) == HIGH) {
      running = true;
    }
  }
  

}
