#define VA A0
#define VB A1

int va_value = 0;
int vb_value = 0;

void setup() {
  pinMode(VA, INPUT);
  pinMode(VB, INPUT);
  Serial.begin(9600);
}

void loop() {
  va_value = analogRead(VA);
  float va_msg = va_value*(5.0/1023.0);
  vb_value = analogRead(VB);
  float vb_msg = vb_value*(5.0/1023.0);

  Serial.print(va_msg-vb_msg);
  Serial.println(" Volts V ");
  
  delay(100);
}
